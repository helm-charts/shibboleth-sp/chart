# Shibboleth SP Helm Chart

Docs: https://int.docs.switch.ch/en/Services/ShibbolethSPHelmChart

Note that the image used by this Helm chart (by default) is built / hosted in a separate project:
https://gitlab.switch.ch/helm-charts/shibboleth-sp/image

## GitLab CI Pipeline:
The pipeline will run separate tasks depending on the type of push:
- Commits:
  - Deploy and test the Helm chart
- Tags:
  - Deploy and test the Helm chart (because we can't enforce a successful commit pipeline run)
  - Package the Helm chart and make it downloadable as an artifact
  - Push the Helm chart to this project's container registry
  - Create a release in this project
