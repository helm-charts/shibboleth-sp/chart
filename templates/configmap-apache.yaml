{{ if not .Values.frontend.apache.baseConfigMap -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "shibboleth-sp.frontend.fullname" . }}-base
  labels:
    {{- include "shibboleth-sp.frontend.labels" . | nindent 4 }}
data:
  base.conf: |
    Include conf-enabled/*.conf
    Include mods-enabled/*.conf
    Include sites-enabled/*.conf
{{ end }}
{{ if not .Values.frontend.apache.mainConfigMap -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "shibboleth-sp.frontend.fullname" . }}-config
  labels:
    {{- include "shibboleth-sp.frontend.labels" . | nindent 4 }}
data:
  000-main.conf: |
    DefaultRuntimeDir run
    PidFile run/apache2.pid

    User www-data
    Group www-data

    Listen 8080
    ServerName _

    TypesConfig /etc/mime.types

    HostnameLookups Off

    # disable forward proxy as having it enabled can pose a security risk
    ProxyRequests off
  {{- if .Values.frontend.apache.extraMainConfig }}
  100-custom.conf: |  
    {{ .Values.frontend.apache.extraMainConfig | nindent 4 }}
  {{- end }}
{{ end }}
{{ if not .Values.frontend.apache.modulesConfigMap -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "shibboleth-sp.frontend.fullname" . }}-modules
  labels:
    {{- include "shibboleth-sp.frontend.labels" . | nindent 4 }}
data:
  000-basics.conf: |
    # These modules are essential
    # LoadModule log_config_module modules/mod_log_config.so # built-in
    # LoadModule unixd_module modules/mod_unixd.so # built-in
    LoadModule authn_core_module modules/mod_authn_core.so
    LoadModule authz_core_module modules/mod_authz_core.so
    LoadModule mime_module modules/mod_mime.so
    LoadModule mpm_event_module modules/mod_mpm_event.so
    LoadModule proxy_module modules/mod_proxy.so
    LoadModule proxy_http_module modules/mod_proxy_http.so
    LoadModule http2_module modules/mod_http2.so
  {{- if .Values.frontend.apache.enableSSL }}
  001-ssl.conf: |
      LoadModule ssl_module modules/mod_ssl.so
  {{- end }}
  010-shibboleth.conf: |
    LoadModule mod_shib modules/mod_shib.so

    # never proxy this special url
    ProxyPass "/Shibboleth.sso" !

    <Location /Shibboleth.sso>
      AuthType None
      Require all granted
    </Location>
  {{- if .Values.frontend.apache.extraModulesConfig }}
  100-custom.conf: |
    {{- .Values.frontend.apache.extraModulesConfig | nindent 4 }}
  {{- end }}
{{ end }}
{{ if not .Values.frontend.apache.sitesConfigMap -}}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "shibboleth-sp.frontend.fullname" . }}-sites
  labels:
    {{- include "shibboleth-sp.frontend.labels" . | nindent 4 }}
data:
  000-proxy.conf: |
    # log levels can be set for mod_shib individually by adding e.g. mod_shib:trace6
    LogLevel {{ .Values.frontend.apache.logLevel | default "warn" }}

    LogFormat {{ .Values.frontend.apache.logFormat | default "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" | quote }} custom
    CustomLog "/dev/stdout" custom
    ErrorLog "/dev/stderr"

    ProxyPreserveHost "{{ .Values.frontend.apache.proxyPreserveHost | default "off" }}"

    {{- if .Values.frontend.apache.enableSSL }}
    # enable HTTP/2 on the frontend
    Protocols h2 http/1.1

    SSLProxyEngine "on"
    {{- end }}

    <VirtualHost _default_:8080>
      UseCanonicalName on
      ServerName https://{{ tpl .Values.global.domainName . }}
      ServerAlias https://*.{{ tpl .Values.global.domainName . }}
      ServerAlias http://{{ tpl .Values.global.domainName . }}
      ServerAlias http://*.{{ tpl .Values.global.domainName . }}

      ProxyPass "/" "{{ tpl .Values.frontend.apache.remoteURL . }}"
      ProxyPassReverse "/" "{{ tpl .Values.frontend.apache.remoteURL . }}"

      <Location />
        # This exception is required as OPTIONS requests come with no cookie
        <LimitExcept OPTIONS>
          AuthType shibboleth
          ShibRequestSetting requireSession true

          # https://www.switch.ch/aai/guides/sp/access-rules/
          {{- if .Values.frontend.apache.accessRules }}
          {{- .Values.frontend.apache.accessRules | nindent 10 }}
          {{- end }}
        </LimitExcept>
      </Location>
    </VirtualHost>
  {{- if .Values.frontend.apache.extraSitesConfig }}
  100-custom.conf: |
    {{- .Values.frontend.apache.extraSitesConfig | nindent 4 }}
  {{- end }}
{{ end }}
