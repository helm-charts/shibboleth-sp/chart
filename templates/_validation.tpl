{{/*
Compile all warnings into a single message, and call fail.
*/}}
{{- define "shibboleth-sp.validateValues" -}}
{{- $messages := list -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.podAntiAffinityPreset" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.ingress" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.eduIDOnly_xor_interfederation" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.eduIDVersion" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.shibboleth.configMap" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.shibboleth.missingValues" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.shibboleth.sessionCache_xor_memcached" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.shibboleth.sessionCache.missingValues" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.backend.shibboleth.memcached.missingValues" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.frontend.apache.mainConfig" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.frontend.apache.modulesConfig" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.frontend.apache.sitesConfig" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.frontend.apache.sitesConfigMap" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.frontend.apache.missingValues" .) -}}
{{- $messages := append $messages (include "shibboleth-sp.validateValues.tests.missingValues" .) -}}
{{- $messages := without $messages "" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\n\nshibboleth-sp - results of values validation:\n%s\n\n" $message | fail -}}
{{- end -}}
{{- end -}}

{{/*************
  *** COMMON ***
  *************/}}

{{- define "shibboleth-sp.validateValues.podAntiAffinityPreset" -}}
{{- if .Values.podAntiAffinityPreset -}}
{{- if not (or (eq .Values.podAntiAffinityPreset "soft") (eq .Values.podAntiAffinityPreset "hard")) }}
  podAntiAffinityPreset
    Illegal value: `podAntiAffinityPreset` must be set to "", "soft" or "hard", not "{{ .Values.podAntiAffinityPreset }}".
{{- end -}}
{{- end -}}
{{- end -}}

{{/*** INGRESS ***/}}

{{- define "shibboleth-sp.validateValues.ingress" -}}
{{- if .Values.ingress.enabled }}

{{- if .Values.ingress.letsEncrypt.enabled }}
{{- if not .Values.ingress.letsEncrypt.clusterIssuer }}
  ingress.letsEncrypt.enabled
  ingress.letsEncrypt.clusterIssuer
    Missing: `clusterIssuer` must be specified if Let's Encrypt is enabled.
{{- end -}}
{{- end -}}

{{- end -}}
{{- end -}}

{{/**************
  *** BACKEND ***
  **************/}}

{{- define "shibboleth-sp.validateValues.backend.eduIDOnly_xor_interfederation" -}}
{{- if and .Values.backend.shibboleth.eduIDOnly .Values.backend.shibboleth.interfederation }}
  backend.shibboleth.eduIDOnly
  backend.shibboleth.interfederation
    Conflict: can't enable both `eduIDOnly` and `interfederation` at the same time.
{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.backend.eduIDVersion" -}}
{{- if not (or (eq .Values.backend.shibboleth.eduIDVersion "production") (eq .Values.backend.shibboleth.eduIDVersion "test")) }}
  backend.shibboleth.eduIDVersion
    Illegal value: `eduIDVersion` must be set to "", "production" or "test", not "{{ .Values.backend.shibboleth.eduIDVersion }}".
{{- end -}}
{{- end -}}

{{/* this is needed independent of whether a configMap is specified or not */}}
{{- if not .Values.backend.shibboleth.applicationDefaults.credentialResolver.activeSecretName }}
  backend.shibboleth.applicationDefaults.credentialResolver.activeSecretName
    Missing: `activeSecretName` must be specified.
{{- end -}}

{{- define "shibboleth-sp.validateValues.backend.shibboleth.configMap" -}}
{{- if .Values.backend.shibboleth.configMap }}

{{- if .Values.backend.shibboleth.version }}
  backend.shibboleth.configMap
  backend.shibboleth.version 
    Conflict: can't specify `version` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.entityID }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.entityID 
    Conflict: can't specify `entityID` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.homeURL }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.homeURL 
    Conflict: can't specify `homeURL` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.remoteUser }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.remoteUser 
    Conflict: can't specify `remoteUser` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.handlerSSL }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.sessions.handlerSSL 
    Conflict: can't specify `handlerSSL` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.sessions.redirectLimit }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.sessions.redirectLimit 
    Conflict: can't specify `redirectLimit` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.sessions.cookieProps }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.sessions.cookieProps 
    Conflict: can't specify `cookieProps` if `configMap` is set.
{{- end -}}

{{- if .Values.backend.shibboleth.applicationDefaults.errors.supportContact }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.errors.supportContact 
    Conflict: can't specify `supportContact` if `configMap` is set.
{{- end -}}

{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.backend.shibboleth.missingValues" -}}
{{- if not .Values.backend.shibboleth.configMap }}

{{- if not .Values.backend.shibboleth.version }}
  backend.shibboleth.configMap
  backend.shibboleth.version
    Missing: `version` must be specified if `configMap` is not specified.
{{- end -}}

{{- if not .Values.backend.shibboleth.applicationDefaults.entityID }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.entityID
    Missing: `entityID` must be specified if `configMap` is not specified.
{{- end -}}

{{- if not .Values.backend.shibboleth.applicationDefaults.homeURL }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.homeURL
    Missing: `homeURL` must be specified if `configMap` is not specified.
{{- end -}}

{{- if not .Values.backend.shibboleth.applicationDefaults.errors.supportContact }}
  backend.shibboleth.configMap
  backend.shibboleth.applicationDefaults.errors.supportContact
    Missing: `supportContact` must be specified if `configMap` is not specified.
{{- end -}}

{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.backend.shibboleth.sessionCache_xor_memcached" -}}
{{- if and .Values.backend.shibboleth.sessionCache.enabled .Values.backend.shibboleth.memcached.enabled }}
  backend.shibboleth.sessionCache.enabled
  backend.shibboleth.memcached.enabled
    Conflict: can't enable both `sessionCache` and `memcached` at the same time.
{{- end -}}
{{- end -}}

{{/*** SESSION CACHE ***/}}

{{- define "shibboleth-sp.validateValues.backend.shibboleth.sessionCache.missingValues" -}}
{{- if .Values.backend.shibboleth.sessionCache.enabled }}

{{- if not .Values.backend.shibboleth.sessionCache.persistedAttributes }}
  backend.shibboleth.sessionCache.persistedAttributes
    Missing: `persistedAttributes` must be specified.
{{- end -}}

{{- if not .Values.backend.shibboleth.sessionCache.sealerKeys.manage }}
{{- if not .Values.backend.shibboleth.sessionCache.sealerKeys.secret }}
  backend.shibboleth.sessionCache.sealerKeys.manage
  backend.shibboleth.sessionCache.sealerKeys.secret
    Missing: `secret` must be specified if `manage` is disabled.
{{- end -}}
{{- end -}}

{{- end -}}
{{- end -}}

{{/*** MEMCACHED ***/}}

{{- define "shibboleth-sp.validateValues.backend.shibboleth.memcached.missingValues" -}}
{{- if and .Values.backend.shibboleth.memcached.enabled (not .Values.backend.shibboleth.memcached.hosts) }}
  backend.shibboleth.memcached.hosts
    Missing: `hosts` must be specified.
{{- end -}}
{{- end -}}


{{/***************
  *** FRONTEND ***
  ***************/}}

{{- define "shibboleth-sp.validateValues.frontend.apache.mainConfig" -}}
{{- if and .Values.frontend.apache.mainConfigMap .Values.frontend.apache.extraMainConfig }}
  frontend.apache.mainConfigMap
  frontend.apache.extraMainConfig
    Conflict: can't specify both `mainConfigMap` and `extraMainConfig`
{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.frontend.apache.modulesConfig" -}}
{{- if and .Values.frontend.apache.mainConfigMap .Values.frontend.apache.extraModulesConfig }}
  frontend.apache.mainConfigMap
  frontend.apache.extraModulesConfig
    Conflict: can't specify both `mainConfigMap` and `extraModulesConfig`
{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.frontend.apache.sitesConfig" -}}
{{- if and .Values.frontend.apache.sitesConfigMap .Values.frontend.apache.extraSitesConfig }}
  frontend.apache.sitesConfigMap
  frontend.apache.extraSitesConfig
    Conflict: can't specify both `sitesConfigMap` and `extraSitesConfig`
{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.frontend.apache.sitesConfigMap" -}}
{{- if .Values.frontend.apache.sitesConfigMap }}

{{- if .Values.frontend.apache.logLevel }}
  frontend.apache.sitesConfigMap
  frontend.apache.logLevel
    Conflict: can't specify `logLevel` if `sitesConfigMap` is set.
{{- end -}}

{{- if .Values.frontend.apache.logFormat }}
  frontend.apache.sitesConfigMap
  frontend.apache.logFormat
    Conflict: can't specify `logFormat` if `sitesConfigMap` is set.
{{- end -}}

{{- if .Values.frontend.apache.remoteURL }}
  frontend.apache.sitesConfigMap
  frontend.apache.remoteURL
    Conflict: can't specify `remoteURL` if `sitesConfigMap` is set.
{{- end -}}

{{- if .Values.frontend.apache.accessRules }}
  frontend.apache.sitesConfigMap
  frontend.apache.accessRules
    Conflict: can't specify `accessRules` if `sitesConfigMap` is set.
{{- end -}}

{{- end -}}
{{- end -}}

{{- define "shibboleth-sp.validateValues.frontend.apache.missingValues" -}}
{{- if not .Values.frontend.apache.sitesConfigMap }}

{{- if not .Values.frontend.apache.remoteURL }}
  frontend.apache.remoteURL
    Missing: `remoteURL` must be specified if `sitesConfigMap` is not specified.
{{- end -}}

{{- if not .Values.frontend.apache.accessRules }}
  frontend.apache.accessRules
    Missing: `accessRules` must be specified if `sitesConfigMap` is not specified.
{{- end -}}

{{- end -}}
{{- end -}}

{{/******************
  *** CHART TESTS ***
  *******************/}}

{{- define "shibboleth-sp.validateValues.tests.missingValues" -}}
{{- if .Values.tests.enabled }}

{{- if not (or .Values.tests.image.repository .Values.tests.image.tag) }}
  tests.enabled
  tests.image.repository
  tests.image.tag
    Missing: `repository` and `tag` must be specified if `enabled` is set to true.
{{- end -}}

{{- end -}}
{{- end -}}
