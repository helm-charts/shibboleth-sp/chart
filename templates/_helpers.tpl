{{/*
Expand the name of the chart.
*/}}
{{- define "shibboleth-sp.name" -}}
{{- default .Chart.Name ( tpl .Values.nameOverride . ) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 54 chars because some Kubernetes name fields are limited to 63 chars (by the DNS naming spec),
and we want to append "-backend" and "-frontend" below.
If release name contains chart name it will be used as a full name.
*/}}
{{- define "shibboleth-sp.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- tpl .Values.fullnameOverride . | trunc 54 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name ( tpl .Values.nameOverride . ) }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 54 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 54 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{- define "shibboleth-sp.backend.fullname" -}}
{{- include "shibboleth-sp.fullname" . }}-backend
{{- end }}

{{- define "shibboleth-sp.frontend.fullname" -}}
{{- include "shibboleth-sp.fullname" . }}-frontend
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "shibboleth-sp.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "shibboleth-sp.labels" -}}
helm.sh/chart: {{ include "shibboleth-sp.chart" . }}
{{ include "shibboleth-sp.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "shibboleth-sp.backend.labels" -}}
helm.sh/chart: {{ include "shibboleth-sp.chart" . }}
{{ include "shibboleth-sp.backend.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "shibboleth-sp.frontend.labels" -}}
helm.sh/chart: {{ include "shibboleth-sp.chart" . }}
{{ include "shibboleth-sp.frontend.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "shibboleth-sp.selectorLabels" -}}
app.kubernetes.io/name: {{ include "shibboleth-sp.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "shibboleth-sp.backend.selectorLabels" -}}
{{ include "shibboleth-sp.selectorLabels" . }}
app.kubernetes.io/component: shibboleth
{{- end }}

{{- define "shibboleth-sp.frontend.selectorLabels" -}}
{{ include "shibboleth-sp.selectorLabels" . }}
app.kubernetes.io/component: apache
{{- end }}

{{/*
Create the names of the service accounts to use
*/}}
{{- define "shibboleth-sp.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- .Values.serviceAccount.name | default (include "shibboleth-sp.fullname" .) }}
{{- else }}
{{- .Values.serviceAccount.name | default "default" }}
{{- end }}
{{- end }}

{{- define "shibboleth-sp.sealerKeys.serviceAccountName" -}}
{{- if .Values.backend.shibboleth.sessionCache.sealerKeys.serviceAccount.create }}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.serviceAccount.name | default (print (include "shibboleth-sp.fullname" .) "-sealer-keys-nanny") }}
{{- else }}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.serviceAccount.name | default "deployer" }}
{{- end }}
{{- end }}

{{/*
Create the names of the images and pull policies to use
*/}}
{{- define "shibboleth-sp.frontend.image.repository" -}}
{{- .Values.frontend.image.repository | default .Values.image.repository -}}
{{- end }}
{{- define "shibboleth-sp.frontend.image.tag" -}}
{{- .Values.frontend.image.tag | default .Values.image.tag -}}
{{- end }}
{{- define "shibboleth-sp.frontend.image" -}}
{{- printf "%s:%s" (include "shibboleth-sp.frontend.image.repository" .) (include "shibboleth-sp.frontend.image.tag" .) }}
{{- end }}
{{- define "shibboleth-sp.frontend.imagePullPolicy" -}}
{{- .Values.frontend.image.pullPolicy | default .Values.image.pullPolicy }}
{{- end }}

{{- define "shibboleth-sp.backend.image.repository" -}}
{{- .Values.backend.image.repository | default .Values.image.repository -}}
{{- end }}
{{- define "shibboleth-sp.backend.image.tag" -}}
{{- .Values.backend.image.tag | default .Values.image.tag -}}
{{- end }}
{{- define "shibboleth-sp.backend.image" -}}
{{- printf "%s:%s" (include "shibboleth-sp.backend.image.repository" .) (include "shibboleth-sp.backend.image.tag" .) }}
{{- end }}
{{- define "shibboleth-sp.backend.imagePullPolicy" -}}
{{- .Values.backend.image.pullPolicy | default .Values.image.pullPolicy }}
{{- end }}

{{- define "shibboleth-sp.sealerKeys.image.repository" -}}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.image.repository | default (include "shibboleth-sp.backend.image.repository" .) -}}
{{- end }}
{{- define "shibboleth-sp.sealerKeys.image.tag" -}}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.image.tag | default (include "shibboleth-sp.backend.image.tag" .) -}}
{{- end }}
{{- define "shibboleth-sp.sealerKeys.image" -}}
{{- printf "%s:%s" (include "shibboleth-sp.sealerKeys.image.repository" .) (include "shibboleth-sp.sealerKeys.image.tag" .) }}
{{- end }}
{{- define "shibboleth-sp.sealerKeys.imagePullPolicy" -}}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.image.pullPolicy | default (include "shibboleth-sp.backend.imagePullPolicy" .)  }}
{{- end }}

{{- define "shibboleth-sp.tests.image" -}}
{{- printf "%s:%s" .Values.tests.image.repository .Values.tests.image.tag }}
{{- end }}
{{- define "shibboleth-sp.tests.imagePullPolicy" -}}
{{- .Values.tests.image.pullPolicy | default .Values.image.pullPolicy }}
{{- end }}

{{/*
Create the names of the secrets to use
*/}}
{{- define "shibboleth-sp.sealerKeys.secretName" -}}
{{- .Values.backend.shibboleth.sessionCache.sealerKeys.secret | default (print (include "shibboleth-sp.fullname" .) "-sealer-keys") }}
{{- end }}
